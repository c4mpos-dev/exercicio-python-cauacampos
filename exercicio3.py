cars = [
    {'marca': 'Audi', 'modelo': 'Q3', 'preco': 180000},
    {'marca': 'Ferrari', 'modelo': 'Italia 458', 'preco': 1500000},
    {'marca': 'FIAT', 'modelo': 'FastBack', 'preco': 120000}
]

contador = 0
print("Bem-vindo(a) à nossa loja!\n")

while True:

    print("\nO que deseja fazer? ")
    print("[1] Comprar carro\n"
          "[2] Vender carro\n"
          "[3] Ver carros\n"
          "[4] Sair da loja\n"
          "\nEscolha uma opção: ")

    choice = int(input())

    if choice == 1:
        print("\nOs carros abaixo estão disponíveis: \n")

        for car in cars:
            print(f'{contador} - {car['marca']} {car['modelo']} | R${car['preco']}')
            contador += 1

        contador = 0

        carro_a_excluir = int(input("\nEscreva o numero do carro para comprar: "))

        for car in cars:
            if carro_a_excluir == cars.index(car):
                cars.remove(car)

        print("\nCarro comprado!")

        #-------------------------------------------------#

    elif choice == 2:

        marca = input("\nDigite a marca do carro: ")
        modelo = input("Digite o modelo do carro: ")
        preco = input("Digite o preço do carro: ")

        cars.append({"marca": marca, "modelo": modelo, "preco": preco})

        print("\nCarros cadastrados com sucesso!")

    elif choice == 3:
        for car in cars:
            print(f'{contador} - {car['marca']} {car['modelo']} | R${car['preco']}')
            contador += 1

        contador = 0

    elif choice == 4:
        break