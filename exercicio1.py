#------------------- Exercício 1 (1)------------------- #

print("EXERCÍCIO 1 \n")

num1 = float(input('Digite um número: '))
num2 = float(input('Digite outro número: '))

if num1 > num2:
    print(f' {num1} é maior que {num2}. \n')
else:
    print(f' {num2} é maior que {num1}. \n')

#------------------- Exercício 2 (1) ------------------- #

print("EXERCÍCIO 2 \n")

pcntg = float(input("Digite o percentual de crescimento da empresa: "))

if pcntg > 0:
    print(f'Sua empresa teve um crescimento de {pcntg}%.')
elif pcntg < 0:
    print(f' Sua empresa teve um decrescimento de {pcntg}%.')
else:
    print("Porcentagem neutra.")