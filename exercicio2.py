#------------------- Exercício 1 (2)  ------------------- #

print("EXERCÍCIO 1 (2) \n")

num1 = int(input('Digite um número inteiro: '))
num2 = int(input('Digite outro número inteiro: '))

print("\nEstes são os números entre eles:")

if num1 < num2:
    for i in range(num1+1, num2):
        print(i)
elif num2 < num1:
    for i in range(num2+1, num1):
        print(i)
else:
    print("Null")

#------------------- Exercício 2 (2)  ------------------- #

print("\nEXERCÍCIO 2 (2)\n")

num1 = int(input('Digite um número inteiro: '))

print(f'\nTabuada do {num1}:')

for i in range(1, 10+1):

    calculo = num1 * i
    print(f'{num1} x {i} = {calculo}')
